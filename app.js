const { app, BrowserWindow, shell, Tray } = require('electron');
const { exec } = require('child_process');
const path = require('path');
const url = require('url');
const fs = require('fs');
const server = require('express')();
const http = require('http').createServer(server);
const io = require('socket.io')(http);
const EventSource = require('eventsource');
const say = require('say');
const port = process.env.PORT || 4525;
const vhServer = require('./vh');

const configPath = app.getPath('userData');

let win;
let tray = null;
let connections = [];
let currentSocket = null;
let vh = null;
let readyToClose = false;
let speaking = false;

function speak(msg) {
    if (speaking === true) {
        return;
    }
    speaking = true;
    console.log(`speak: "${msg}"`)
    if (process.platform === 'win32') {
        say.speak(msg, null, 1.0, (err) => {
            if (err) {
                return;
            }
            speaking = false;
        });
    } else if (process.platform === 'linux') {
        exec(
            '/usr/bin/mplayer -ao alsa -really-quiet -noconsolecontrols "http://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q=' + msg + '&tl=en"',
            (err, stdout, stderr) => {
                if (err) {
                    console.log(err);
                    return;
                }
                speaking = false;
            }
        );
    }
}

speak('Welcome to the Hollow');

function numberOfConnections() {
    return Object.keys(connections).length;
}

function createWindow() {
    const trayIcon = __dirname + '/public/images/icon.png';
    console.log(`tray name? ${trayIcon}`);
    tray = new Tray(trayIcon);
    win = new BrowserWindow({
        webPreferences: {
            sandbox: false,
            nodeIntegration: true
        },
        width: 1280,
        height: 960,
        icon: __dirname + '/public/images/icon.png',
        show: true
    });

    win.setMenuBarVisibility(false);

    // win.webContents.openDevTools();

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'public/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    win.webContents.executeJavaScript('window.config = { port: ' + port + ' };');

    win.on('closed', () => {
        win = null;
    });

    win.on('close', (e) => {
        console.log('pre close')
        console.log('window closed, time to log out');
        if (vh !== null) {
            if (vh.loggedIn) {
                vh.logout(() => {
                    console.log('logout callback');
                    readyToClose = true;
                    win.close();
                });
            } else {
                readyToClose = true;
            }
        } else {
            readyToClose = true;
        }
        if (!readyToClose) {
            e.preventDefault();
        }
    });
}

processEvent = function (data) {
    if (data.type === 'USERLIST_UPDATE') {
        console.log('sending `userjoined` event to client')
        console.log(data);
        currentSocket.emit('userjoined', data);
    }
    if (data.type === 'USERLIST_REMOVE') {
        console.log('sending `userleft` event to client')
        console.log(data);
        currentSocket.emit('userleft', data);
    }
    if ((data.type === 'ROOM_CHAT' || data.type === 'WHISPER_CHAT')) {
        currentSocket.emit('chatpost', data);
    }
    if (data.type === 'DEL_MESSAGE') {
        currentSocket.emit('deletemessage', data);
    }
};

connectEventStream = function (lastEventId) {
    // Subscribe to the event stream
    let eventStreamUrl = 'https://vh.mucktopia.com/u/' + vh.chatId + '/events.stream?lastEventId=' + lastEventId;
    //console.log('starting event stream ' + eventStreamUrl);
    let my_cookies = vh.cookieJar.getCookiesSync('https://vh.mucktopia.com/u/' + vh.chatId);
    let my_cookie_strings = [];
    for (let i = 0; i < my_cookies.length; i += 1) {
        my_cookie_strings.push(my_cookies[i].cookieString());
    }
    let cookie_string = my_cookie_strings.join('; ');
    //console.log('cookie string: ' + cookie_string);
    es = new EventSource(eventStreamUrl, {
        withCredentials: true,
        headers: {
            cookie: cookie_string,
            referer: 'https://vh.mucktopia.com/u/' + vh.chatId + '/jchat.srv'
        }
    });
    es.onopen = function (e) {
    };
    es.onmessage = function (e) {
        let data = JSON.parse(e.data);
        console.log(data);
        processEvent(data);
    };
    es.onerror = function (e) {
    };
};

io.on('connect', (socket) => {
    currentSocket = socket;
    // New connection established, welcome the socket
    connections[socket.id] = socket;
    console.log(`A socket [${socket.id}] connected, number of active connections: ${numberOfConnections()}`)
    socket.emit('welcome', {
        message: 'Welcome to the VH daemon'
    });

    socket.on('error', (error) => {
    });

    socket.on('disconnecting', (reason) => {
    });

    // When the connection goes down, we clear it from the connections
    socket.on('disconnect', () => {
        vh = null;
        delete connections[socket.id];
        socket.disconnect();
        console.log(`A socket disconnected, ${numberOfConnections()} active connections left`);
    });

    // Actual events
    socket.on('vhlogin', (data) => {
        vh = new vhServer(socket, data.username, data.password, (characters) => {
            console.log('got characters');
            console.log(characters);
            speak('Please pick your character');
            socket.emit('loggedin', {
                characters: characters
            });
        });
    });

    socket.on('startchat', (data) => {
        vh.loginCharacter(data.character, () => {
            vh.initialChat(() => {
                vh.userlist();
            });
        });
    });

    socket.on('notify', (data) => {
        console.log('send notification');
        console.log(data);
        app.setBadgeCount(app.getBadgeCount() + 1);
    });

    socket.on('speak', (data) => {
        speak(data.message);
    });

    socket.on('profile', (data) => {
        shell.openExternal('https://vh.mucktopia.com/profile/' + data.name);
    });

    socket.on('openlink', (data) => {
        shell.openExternal(data.url);
    });

    socket.on('savesettings', (data) => {
        console.log('saving settings');
        console.log(data);
        // Persist all the relationships we have... into a file?
        fs.writeFileSync(configPath + '/settings.json', JSON.stringify(data));
    });

    socket.on('loadsettings', (nop) => {
        // Load all the relationships we have from a file?
        fs.readFile(configPath + '/settings.json', (err, data) => {
            if (err) {
                socket.emit('settings', {
                    'auth': {},
                    'relations': {}
                });
                return;
            }
            // Send the data
            console.log('loaded settings');
            console.log(JSON.parse(data));
            socket.emit('settings', JSON.parse(data));
        });
    });

    socket.on('postmessage', (data) => {
        vh.sendMessage(data.post, data.to, data.eyecon);
    });

    socket.on('beginpolling', (data) => {
        let lastEventId = data.lastEventId;
        connectEventStream(lastEventId);
    });

    socket.on('fetchuserlist', (data) => {
        vh.userlist();
    });

    socket.on('setstatus', (data) => {
        vh.setStatus(data.code);
    });
});

http.listen(port, () => {
    console.log(`VH daemon running on port ${port}`)
});

app.on('ready', createWindow);

app.setAppUserModelId("com.gryphonmedia.vhdesktop");

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
