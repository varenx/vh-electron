import Login from './components/Login.vue';
import LoggingIn from './components/LoggingIn.vue';
import LoadingScreen from './components/LoadingScreen.vue';
import CharSelect from './components/CharSelect.vue';
import StartSession from './components/StartSession.vue';
import Chat from './components/Chat.vue';
import Vue from 'vue';
import io from 'socket.io-client';
import $ from 'jquery';
require('../../node_modules/materialize-css/dist/js/materialize.min.js');

const port = VHE.config.port;

console.log(`port is ${port}`);

M.AutoInit();

new Vue({
    el: '#app',
    components: {
        Login,
        LoggingIn,
        CharSelect,
        StartSession,
        Chat
    },
    data: {
        socket: null,
        messages: [],
        characters: [],
        online: [],
        onlineUpdateBuffer: [],
        onlineFrozen: false,
        eyecons: [],
        currentEyecon: null,
        motd: '',
        roomBg: '',
        rooms: [],
        roomName: '',
        statuses: [],
        status: 'online',
        currentScreen: 'login',
        lastEventId: 0,
        currentChar: '',
        settings: {
            auth: {},
            relations: {}
        },
        rpMode: false,
        tabs: [],
        activeTabId: ''
    },
    computed: {
        activeScreen() {
            // TODO: We should really check if the user has
            // entered account details
            return this.currentScreen;
        },
        currentProperties() {
            let me = this;
            if (me.currentScreen == 'chat') {
                return {
                    tabs: me.tabs,
                    online: me.online,
                    eyecons: me.eyecons
                };
            }
            if (me.currentScreen == 'char-select') {
                return { 'characters': me.characters };
            }
            return {};
        }
    },
    created() {
        var me = this;
        me.socket = io('http://localhost:' + port);
    },
    mounted() {
        var me = this;

        // Add jQuery dom handler, we want all chat links (in the room)
        // to open in the browser
        $(document).on('click', '.message-body a', (e) => {
            let openUrl = $(e.target).attr('href');
            e.preventDefault();
            me.socket.emit('openlink', {
                url: openUrl
            });
        });

        $(document).on('click', '.message-body span.spoiler', (e) => {
            $(e.target).toggleClass('view-spoiler');
        });

        me.$on('setstatus', (code) => {
            me.status = code;
            me.socket.emit('setstatus', {
                code: code
            });
        });

        me.$on('savesettings', () => {
            me.socket.emit('savesettings', me.settings);
        });

        me.$on('deleteMessage', (messageId) => {
            this.deleteMessage(messageId);
        });


        me.socket.on('connect', () => {
            console.log(`Connected to server`);

            me.socket.on('loggedin', (data) => {
                console.log('Account logged in');
                this.characters = data.characters;
                this.currentScreen = 'char-select';

                // Load relations
                me.socket.emit('loadsettings', {});
            });

            me.socket.on('vhrelations', (data) => {
                let mentions = this.settings.relations[this.currentChar].mentions;
                let vhMentions = data.mentions;
                for (let i = 0; i < vhMentions.length; i += 1) {
                    if (mentions.indexOf(vhMentions[i]) === -1) {
                        mentions.push(vhMentions[i]);
                    }
                }
            });

            me.socket.on('deletemessage', (data) => {
                this.deleteMessage(data.deletedMsgId);
            });

            me.socket.on('chatpost', (data) => {
                let notified = false;
                if (this.settings.relations[this.currentChar].ignores.indexOf(data.from.name) !== -1) {
                    return; // Just ignore the message entirely
                }
                if (data.from.name === 'System') {
                    // Is it the status change message
                    if (data.body === 'You have been idle for too long, setting your status.') {
                        // Just ignore
                        // In fact, first we set our status back, THEN ignore
                        this.$emit('setstatus', 'online');
                        return;
                    }
                }
                // Mentions...
                // If the user is highlighted, notify!
                if (data.type === 'ROOM_CHAT') {
                    if (this.settings.relations[this.currentChar].highlights.indexOf(data.from.name) !== -1) {
                        // Notify the user
                        notified = true;
                        let body = me.processPostText(data);
                        let notification = {
                            title: data.from.name + ' posted in chat',
                            body: body
                        }
                        data.highlighted = true;
                        this.notify(notification, 'highlight');
                        this.speak(body.replace(/([a-zA-Z0-9_]+): /, data.from.name + ' says '));
                    }
                }
                this.addMessageToTab(data);
                // Check mentions
                if (notified === false) {
                    let message = me.processPostText(data, false);
                    let mentions = me.settings.relations[me.currentChar].mentions;
                    for (let i = 0; i < mentions.length; i += 1) {
                        let rege = new RegExp(mentions[i], 'im');
                        let res = message.match(rege);
                        if (res !== null) {
                            // Time to notify, then break
                            let body = me.processPostText(data);
                            let notification = {
                                title: data.from.name + ' mentioned you in the chat',
                                body: body
                            };
                            this.notify(notification, 'mention');
                            notified = true;
                            break;
                        }
                    }
                }
                if (notified === false) {
                    if ((data.type === 'WHISPER_CHAT') && (data.from.name != this.currentChar)) {
                        // Notify the user
                        let body = me.processPostText(data);
                        let notification = {
                            title: data.from.name + ' sent you a whisper',
                            body: body
                        };
                        this.notify(notification, 'whisper');
                    }
                }
                this.lastEventId = data.id;
            });

            me.socket.on('userjoined', (data) => {
                let found = false;

                if (typeof data.add === 'undefined') {
                    return;
                }

                // First of all, we need to check if the user is already on
                // the list, by char id
                for (let i = 0; i < this.online.length; i += 1) {
                    if (this.online[i].charId === data.add.charId) {
                        found = true;
                        // User is already online, modify the data
                        if (this.onlineFrozen === false) {
                            this.$set(this.online, i, data.add);
                        } else {
                            this.onlineUpdateBuffer.push({
                                command: 'update',
                                charId: data.add.charId,
                                data: data.add
                            });
                        }
                        break;
                    }
                }
                if (!found) {
                    // Check if the user is a friend, if it is, notify
                    if (this.settings.relations[this.currentChar].friends.indexOf(data.add.charName) !== -1) {
                        let notification = {
                            title: data.add.charName + ' joined',
                            body: data.add.charName + ' just came online'
                        }
                        this.notify(notification, 'highlight');
                    }
                    // Check if frozen
                    if (this.onlineFrozen === false) {
                        this.online.push(data.add);
                    } else {
                        this.onlineUpdateBuffer.push({
                            command: 'add',
                            data: data.add
                        });
                    }
                }
                this.lastEventId = data.id;
            });

            me.socket.on('userleft', (data) => {
                let leaverName = '';
                for (let i = 0; i < this.online.length; i += 1) {
                    let char = this.online[i];
                    if (char.sessionId === data.remove) {
                        leaverName = char.charName;
                        if (this.onlineFrozen === false) {
                            this.online.splice(this.online.indexOf(char), 1);
                        } else {
                            this.onlineUpdateBuffer.push({
                                command: 'remove',
                                data: char
                            });
                        }
                    }
                }
                if (leaverName !== '') {
                    // Check if it's friend, if it is, notify
                    if (this.settings.relations[this.currentChar].friends.indexOf(leaverName) !== -1) {
                        let notification = {
                            title: leaverName + ' left',
                            body: leaverName + ' just left the room'
                        }
                        this.notify(notification, '');
                    }
                }
                this.lastEventId = data.id;
            });

            me.socket.on('userlist', (data) => {
                this.online = [];
                for (let i = 0; i < data.length; i += 1) {
                    this.online.push(data[i]); // the actual character
                }
                me.socket.emit('beginpolling', {
                    lastEventId: me.lastEventId
                });
            });

            me.socket.on('initialchat', (data) => {
                this.currentScreen = 'chat';
                let i = 0;
                // Let's check the relations settings, init
                if (typeof this.settings.relations[this.currentChar] === 'undefined') {
                    this.settings.relations[this.currentChar] = {
                        ignores: [],
                        friends: [],
                        highlights: [],
                        mentions: []
                    };
                }
                // Create the room tab
                if (me.tabs.length === 0) {
                    let firstTabId = 'tab-' + data.roomPrettyName.toLowerCase().replace(/[^a-z0-9-]+/gim, '-');
                    me.tabs.push({
                        id: firstTabId,
                        type: 'public',
                        title: data.roomPrettyName,
                        messages: [],
                        unread: 0
                    });
                    this.activeTabId = firstTabId;
                }
                for (i = 0; i < data.initialChat.length; i += 1) {
                    let msg = data.initialChat[i];
                    // Check for mute
                    if (me.settings.relations[this.currentChar].ignores.indexOf(msg.from.name) !== -1) {
                        continue; // Just ignore and carry on!
                    }
                    if (me.settings.relations[this.currentChar].highlights.indexOf(msg.from.name) !== -1) {
                        msg.highlighted = true;
                    }
                    this.addMessageToTab(msg, true); // true = initial load
                }
                me.lastEventId = data.lastMsgId;
                me.eyecons = data.favColors;
                me.currentEyecon = me.eyecons[0];
                me.motd = data.motd;
                me.roomBg = data.roomBgImage;
                me.rooms = data.roomList;
                me.roomName = data.roomPrettyName;
                this.$set(this, 'statuses', data.statusList);
            });

            me.socket.on('settings', (data) => {
                console.log('settings');
                console.log(data);
                me.settings = data;
            });
        });

        me.socket.on('disconnect', () => {
            console.log(`Disconnected from server`);
        });
    },
    methods: {
        login (username, password) {
            var me = this;

            me.currentScreen = 'logging-in';

            me.socket.emit('vhlogin', {
                username: username,
                password: password
            })
        },
        loginCharacter (name) {
            var me = this;

            // Switch to the loading screen
            me.currentScreen = 'start-session';

            me.currentChar = name;

            me.socket.emit('startchat', {
                character: name
            });
        },
        postMessage (post, to) {
            let me = this;

            console.log('sending message ' + post + ' to ' + to);
            me.socket.emit('postmessage', {
                post: post,
                to: to,
                eyecon: this.currentEyecon.id
            });
        },
        notify (notification, audio) {
            audio = (typeof audio === 'undefined') ? '' : audio;
            if (audio !== '') {
                document.getElementById('notify-' + audio).play();
            }
            new Notification(notification.title, {
                body: notification.body
            });
            me.socket.emit('notify', {
                title: notification.title,
                body: notification.body
            });
        },
        speak (message) {
            let me = this;

            console.log(`speak: ${message}`)

            me.socket.emit('speak', {
                message: message
            });
        },
        processPostText (data, full) {
            let body = '';
            full = (typeof full === 'undefined') ? true : full;

            if (full) {
                if (data.isPose) {
                    body = data.from.name + ' ' + data.body;
                } else {
                    body = data.from.name + ': ' + data.body;
                }
            } else {
                body = data.body;
            }
            // Strip all html
            body = body.replace(/<[^>]*>?/gim, '');
            // Unescape
            body = body.replace(/&amp;/gim, '&');
            body = body.replace(/&quot;/gim, '"');
            body = body.replace(/&apos;/gim, "'");
            body = body.replace(/&gt;/gim, ">");
            body = body.replace(/&lt;/gim, "<");


            return body;
        },
        getTabById(id) {
            let tab = null

            for (let i = 0; i < this.tabs.length; i += 1) {
                if (this.tabs[i].id === id) {
                    tab = this.tabs[i];
                    break;
                }
            }
            return tab;
        },
        getStatus() {
            var me = this;

            for (let i = 0; i < me.statuses.length; i += 1) {
                if (me.statuses[i].code === me.status) {
                    return me.statuses[i];
                }
            }
            return { code: 'online', name: 'Online' };
        },
        deleteMessage(messageId) {
            for (let i = 0; i < this.tabs.length; i += 1) {
                let currentTab = this.tabs[i];

                for (let j = 0; j < currentTab.messages.length; j += 1) {
                    let currentMsg = currentTab.messages[j];
                    if (typeof currentMsg === 'undefined') {
                        continue;
                    }
                    if (currentMsg.id === messageId) {
                        currentTab.messages.splice(currentTab.messages.indexOf(currentMsg), 1);
                    }
                }
            }
        },
        addMessageToTab(msg, initial) {
            let toTab = null;
            let tabName = '';
            let newTab = false;
            initial = (typeof initial === 'undefined') ? false : true;
            if (msg.type === 'WHISPER_CHAT') {
                if (msg.from.name === 'System') {
                    return false;
                }
                tabName = (msg.from.name === this.currentChar) ? msg.to.name : msg.from.name;
            } else {
                // TODO: Check this, why only prettyName exists in initial load
                // but not name?
                tabName = msg.to.prettyName.toLowerCase().replace(/[^a-z0-9-]+/gim, '-');
            }
            let toId = 'tab-' + tabName.toLowerCase();

            for (let i = 0; i < this.tabs.length; i += 1) {
                if (this.tabs[i].id === toId) {
                    toTab = this.tabs[i];
                    break;
                }
            }
            if (toTab === null) {
                newTab = true;

                // Create tab
                if (msg.type === 'WHISPER_CHAT') {
                    if (!initial) {
                        toTab = {
                            id: toId,
                            type: 'private',
                            title: tabName,
                            messages: [],
                            unread: 0
                        };
                    } else {
                        toTab = this.tabs[0];
                    }
                } else {
                    toTab = {
                        id: toId,
                        type: 'public',
                        title: msg.to.prettyName,
                        messages: [],
                        unread: 0
                    };
                }
                if (!initial) {
                    this.tabs.push(toTab);
                }
                this.$nextTick(() => {
                    var me = this;
                    M.Tabs.init(document.getElementById('chat-tabs'), {
                        onShow(tab) {
                            let tabs = this;
                            // Set the unread field back to 0...
                            let tabShown = me.getTabById(tab.id);
                            console.log('showing tab: ' + tab.id);
                            console.log(tabShown);
                            if (tabShown !== null) {
                                me.activeTabId = tab.id;
                                tabShown.unread = 0;
                                // Scroll down to the bottom when you switch...
                                let chatLog = $('#' + tab.id + ' .chat-messages')[0];
                                chatLog.scrollTop = chatLog.scrollHeight - chatLog.clientHeight;
                                me.$nextTick(() => {
                                    tabs.updateTabIndicator();
                                });
                            }
                        }
                    });
                });
            }
            toTab.messages.push(msg);

            if (newTab === true || initial === true) {
                this.$nextTick(() => {
                    setTimeout(() => {
                        let chatLog = document.querySelectorAll('#' + toId + ' .chat-messages')[0];
                        if (chatLog !== undefined) {
                            chatLog.scrollTop = chatLog.scrollHeight - chatLog.clientHeight;
                        }
                    }, 300);
                });
            }

            // Increase the number of unread messages, but only if not active..?
            // And only if it's not the initial load, otherwise
            // we can safely assume the messages have been read
            if (!initial) {
                if (this.activeTabId != toTab.id) {
                    toTab.unread += 1;
                    this.$nextTick(() => {
                        let tabs = M.Tabs.getInstance(document.getElementById('chat-tabs'));
                        tabs.updateTabIndicator();
                    });
                }
            }

        }
    }
});
