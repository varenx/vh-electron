const axios = require('axios').default;
const cjSupport = require('axios-cookiejar-support').default;
const tough = require('tough-cookie');
const qs = require('querystring');

const url = 'https://vh.mucktopia.com';
cjSupport(axios);

module.exports = class vh {
    constructor(socket, username, password, cb) {
        this.username = username;
        this.password = password;
        this.cookieJar = new tough.CookieJar();
        this.availableCharacters = [];
        this.chatId = '';
        this.mySession = {};
        this.mentions = [];
        this.cb = cb;
        this.socket = socket;
        this.chatData = {};
        this.loggedIn = false;

        // Start the login process
        this.login();
    }

    login() {
        let me = this;
        axios.get(url, {}).catch(() => {}).then((res) => { me.login1(res); });
    }

    login1(res) {
        console.log(`login1`)
        let me = this;
        let cookieJar = me.cookieJar;
        axios.get(url + '/login.srv?mode=gotoSso&provider=vh.mucktopia.com', {
            jar: cookieJar,
            withCredentials: true
        })
            .catch(() => {})
            .then((res) => { me.login2(res); })
    }

    login2(res) {
        console.log(`login2`);
        let me = this;
        let cookieJar = me.cookieJar;
        let response = res.data;
        let rege = new RegExp('name="redir" value="([^"]+)"');
        let matches = rege.exec(response);
        let redir_url = '';
        if (typeof matches[1] !== 'undefined') {
            redir_url = matches[1];
        }
        redir_url = redir_url.replace(/&amp;/, '&');
        // Now let's fetch the redirection address
        const data = qs.stringify({
            redir: redir_url,
            username: me.username,
            password: me.password
        });

        axios.post(url + '/auth/login.srv?mode=login', data, {
            jar: cookieJar,
            withCredentials: true
        })
            .then((res) => { me.fetchCharacters(res); })
            .catch((err) => {
                console.log('----------------- ERROR ---------------');
                console.log(err);
            });
    }

    fetchCharacters(res) {
        let me = this;
        let rege = new RegExp('for="charName_([0-9]+)" name="charName" value="([a-zA-Z0-9_]+)"', 'gim');
        let response = res.data;
        let charStrings = [];
        let matches = [];
        do {
            matches = rege.exec(response);
            if (matches) {
                me.availableCharacters.push({
                    name: matches[2]
                });
            }
        } while (matches);
        me.cb(me.availableCharacters);
    }

    loginCharacter(name, cb) {
        let me = this;
        let cookieJar = me.cookieJar;
        /*
        if (me.availableCharacters.indexOf(name) === -1) {
            // This character doesn't exist...?
            console.log(`Error, ${name} is not a valid character`);
            return false;
        }
        */
        console.log(`Logging in with character ${name}`);
        const data = qs.stringify({
            command: 'charLogin',
            room: 2,
            charName: name
        });
        axios.post(url + '/account.srv', data, {
            jar: cookieJar,
            withCredentials: true
        })
            .then((res) => {
                me.fetchSessionData(res, cb);
            });
    }

    fetchSessionData(res, cb) {
        let me = this;
        let rege = /<script id="session-settings" type="application\/json"\s*>([^<]+)<\/script>/gim;
        let matches = rege.exec(res.data);
        let my_session = JSON.parse(matches[1]);
        rege = /https:\/\/vh\.mucktopia.com\/u\/([a-zA-Z0-9_-]+)\/jchat\.srv/;
        matches = rege.exec(res.config.url);
        if (matches === null) {
            console.log('Error: couldn\'t get session id');
            console.log(res.config.url);
            console.log(res.data);
            return;
        }
        me.chatId = matches[1];
        me.mySession = my_session;
        me.mentions = me.mySession.me.settings.nameDingPatterns;
        me.loggedIn = true;
        cb();
    }

    logout(cb) {
        let me = this;
        let cookieJar = me.cookieJar;
        let urlLogout = url + '/u/' + me.chatId + '/action.srv?action=logout';

        if (!me.loggedIn) {
            return;
        }

        axios.post(urlLogout, qs.stringify({
            csrf: me.mySession.me.csrf
        }), {
            jar: cookieJar,
            withCredentials: true
        })
            .catch((err) => { console.log('ERROR WITH LOGOUT'); console.log(err); cb(); })
            .then(
                (res) => { console.log('Successfully logged out'); cb(); }
            )
    }

    initialChat(cb) {
        let me = this;
        let cookieJar = me.cookieJar;

        axios.get(url + '/u/' + me.chatId + '/info.srv?dataset=initialchat-all', {
            jar: cookieJar,
            withCredentials: true
        })
            .then((res) => {
                me.chatData = res.data.result;
                console.log(`initialchat message being sent...`)
                me.socket.emit('initialchat', me.chatData);
                me.socket.emit('vhrelations', {
                    mentions: me.mentions
                });
                cb();
            });
    }

    sendMessage(message, to, eyecon) {
        let me = this;
        let msgTo = (to === undefined) ? '' : to;
        let cookieJar = me.cookieJar;

        console.log('send message');
        console.log(message + ' to ' + msgTo);

        axios.post(url + '/u/' + me.chatId + '/action.srv', qs.stringify({
            csrf: me.mySession.me.csrf,
            action: 'post',
            msgTarget: msgTo,
            color: eyecon,
            msgBody: message
        }), {
            jar: cookieJar,
            withCredentials: true
        })
            .then(
                (res) => {
                    // We posted, do nothing
                }
            ); // /axios post
    }

    userlist() {
        let me = this;
        let cookieJar = me.cookieJar;

        axios.get(url + '/u/' + me.chatId + '/info.srv?dataset=userlist', {
            jar: cookieJar,
            withCredentials: true
        })
            .then((res) => {
                let userlist = res.data.result;
                console.log(`userlist message being sent...`);
                me.socket.emit('userlist', userlist);
            });
    }

    setStatus(code) {
        let me = this;
        let cookieJar = me.cookieJar;

        axios.post(url + '/u/' + me.chatId + '/action.srv', qs.stringify({
            csrf: me.mySession.me.csrf,
            action: 'setStatus',
            newStatus: code
        }), {
            jar: cookieJar,
            withCredentials: true
        })
            .then(
                (res) => {}
            ); // /axios post
    }

};
