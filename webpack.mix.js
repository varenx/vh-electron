const mix = require('laravel-mix');

mix.sourceMaps().js('assets/js/app.js', 'public/js/app.js');
mix.sourceMaps().sass('assets/sass/app.scss', 'public/css/app.css');
